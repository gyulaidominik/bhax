public class Delivery {
    public static void main(String[] args) {

        DeliveryFactory deliveryFactory = new GroundDeliveryFactory();

        TransportCompanies transporter = deliveryFactory.createDeliveryPlan(500);

        System.out.println(transporter);

    }
}
