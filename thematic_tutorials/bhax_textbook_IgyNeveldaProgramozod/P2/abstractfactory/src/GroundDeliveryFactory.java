public class GroundDeliveryFactory implements DeliveryFactory {



    @Override
    public TransportCompanies createDeliveryPlan(Integer distance) {

        TransportCompanies transporter;

        if (distance < 500) {
            transporter = TransportCompanies.GLS;

        }
        else {
                transporter = TransportCompanies.DPD;
            }

        return transporter;

    }


}