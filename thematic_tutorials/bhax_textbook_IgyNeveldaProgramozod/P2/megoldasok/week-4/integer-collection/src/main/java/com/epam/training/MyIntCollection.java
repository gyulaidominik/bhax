package com.epam.training;

public class MyIntCollection {

    int[] array;
    int size;
    int index = 0;
    boolean sorted = false;

    public MyIntCollection(int size){
        this.size = size;
        this.array = new int[this.size];
    }

    public MyIntCollection(MyIntCollection intcollection){
        this.array = intcollection.array;
        this.size = intcollection.size;
        this.index = intcollection.index;
        this.sorted = intcollection.sorted;
    }

    public MyIntCollection(int[] arrayOfInts){
        this.array = arrayOfInts;
        this.size = arrayOfInts.length;
        this.index = this.size;
        this.sorted = false;
    }

    public void add(int number){
        if(size<=index) throw new IllegalArgumentException("The collection is full");

        sorted = false;
        array[index++] = number;
    }

    public boolean contains(int searchArg){

        if(!sorted){
            sort();
        }
        int left = 0;
        int right = size - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;

            if (array[mid] == searchArg) {
                return true;
            }
            else if (array[mid] < searchArg) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return false;
    }

    public int[] sort(){

        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - i - 1; j++) {

                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        sorted = true;
        return array;
    }

}
