package com.epam.training;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import com.epam.training.asciiprinter.AsciiPrinter;

public class Main {

	public static void main(String[] args) throws IOException {
		String imagePath = args[0];
		String textFilePath = args.length != 2 ? null : args[1];
		OutputStream outputStream = textFilePath == null ? System.out : new FileOutputStream(textFilePath);
		BufferedImage image = ImageIO.read(new File(imagePath));

		new AsciiPrinter(outputStream, image).print();
	}

}
