package com.epam.training;

import java.io.IOException;

import com.epam.training.coder.stream.ConsoleInputToFileCeasarEncoder;
import com.epam.training.coder.stream.StreamEncoder;

public class Main {

	public static void main(String[] args) throws IOException {
		String fileName = "encoded.txt"; //args[0];
		int offset = 5;//Integer.valueOf(args[1]);
		try (StreamEncoder handler = new ConsoleInputToFileCeasarEncoder(fileName, offset)) {
			handler.handleInputs();
		}
	}

}
