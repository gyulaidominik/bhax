package com.epam.training.coder.ceasar;

import java.util.function.Function;

import com.epam.training.coder.Decoder;
import com.epam.training.coder.Encoder;

public class CeasarCoder implements Encoder, Decoder {

	private int offset;
	private static final int BACKTO_FIRSTLETTER = 102;

	public CeasarCoder(int offset) {
		if (offset < 1 || offset > 26) {
			throw new IllegalArgumentException("Offset must be between 1 and 26");
		}
		this.offset = offset;
	}

	public CeasarCoder() {
		this.offset = 1;
	}

	@Override
	public String decode(String text) {
		return buildString(text, character -> (char) ((character - offset) % 128));
	}

	@Override
	public String encode(String text) {
		return buildString(text, character -> (char) ((character + offset) % 128));
	}

	private char skipSpecialChars(char character){
				return (char)((int)character+BACKTO_FIRSTLETTER);
	}

	private char secureAbcShift(char character){
		int encodedChar = character+offset;
		if(encodedChar>122){
			character = skipSpecialChars(character);
		}
		else if(character <91 && encodedChar > 91){
			character = skipSpecialChars(character);
		}
		return character;
	}

	private String buildString(String text, Function<Character, Character> function) {
		StringBuilder result = new StringBuilder();
		for (char character : text.toCharArray()) {
			if (character != ' ') {

				character =	secureAbcShift(character);

				result.append(function.apply(character));
			} else {
				result.append(character);
			}
		}
		return result.toString();
	}

}
