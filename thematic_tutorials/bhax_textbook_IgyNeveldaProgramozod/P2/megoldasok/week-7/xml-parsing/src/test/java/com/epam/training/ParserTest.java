package com.epam.training;

import java.io.FileNotFoundException;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;

import com.epam.training.exercise.ParserExercise;
import org.junit.jupiter.api.Test;

import com.epam.training.solution.ParserSolution;

public class ParserTest {

    private final ParserExercise underTest = new ParserExercise();

    @Test
    public void testParse() throws TransformerException, XMLStreamException, FileNotFoundException {
        underTest.parse();
    }

}
