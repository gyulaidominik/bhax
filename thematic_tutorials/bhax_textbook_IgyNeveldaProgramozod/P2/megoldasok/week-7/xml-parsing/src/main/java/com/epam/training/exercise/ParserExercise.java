package com.epam.training.exercise;

import org.apache.batik.anim.dom.SVGDOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class ParserExercise {


    private Map<String, String> colorsOfStates;
    private static final String INPUT_FILE = "src/main/resources/input.xml";
    private static final String OUTPUT_FILE = "src/main/resources/result.svg";


    public void parse() throws XMLStreamException, TransformerException, FileNotFoundException {
        List<City> cityList = createCityList();
        colorsOfStates = createColorsOfStates(cityList);
        Document document = createDOMSource(cityList);

        TransformerFactory.newInstance().newTransformer().transform(new DOMSource(document), new StreamResult(OUTPUT_FILE));
    }

    private Document createDOMSource(List<City> cityList) {
        Document document = SVGDOMImplementation.getDOMImplementation()
                                                .createDocument(SVGDOMImplementation.SVG_NAMESPACE_URI, "svg", null);
        Element rootElement = document.getDocumentElement();
        rootElement.setAttributeNS(null, "width", "800");
        rootElement.setAttributeNS(null, "height", "600");

        cityList.forEach(yourModel -> {
            Element element = document.createElementNS(SVGDOMImplementation.SVG_NAMESPACE_URI, "circle");
            // cy -> X koordináta értéke!
            element.setAttributeNS(null, "cy", yourModel.getCoordinateX());
            // cx -> Y koordináta értéke!
            element.setAttributeNS(null, "cx", yourModel.getCoordinateY());
            // fill -> random hexadecimális szín!
            element.setAttributeNS(null, "fill", colorsOfStates.get(yourModel.getState()));
            // r -> a pont átmérőjét jelenti -> legyen 1!
            element.setAttributeNS(null, "r", "1");
            rootElement.appendChild(element);
        });
        return document;
    }


    private Map<String, String> createColorsOfStates(List<City> cityList){
        return cityList.stream()
                .map(City::getState)
                .distinct()
                .collect(Collectors.toMap(stateName -> stateName, hexColor -> randomHexColor()));
    }


    private List<City> createCityList() throws FileNotFoundException, XMLStreamException {
        XMLStreamReader xmlStreamReader = XMLInputFactory.newFactory().createXMLStreamReader(new FileInputStream(INPUT_FILE));
        List<City> cityList = new LinkedList<>();
        City.CityBuilder builder = new City.CityBuilder();
        while(xmlStreamReader.hasNext()){
            int actual = xmlStreamReader.next();
             if(isStartElement(actual, "coordinateY", xmlStreamReader)){
                 builder.coordinateY(xmlStreamReader.getElementText());
             } else if(isStartElement(actual, "coordinateX", xmlStreamReader)){
                 builder.coordinateX(xmlStreamReader.getElementText());
             } else if(isStartElement(actual, "state", xmlStreamReader)){
                 builder.state(xmlStreamReader.getElementText());
             } else if (isEndELement(actual, "city", xmlStreamReader)){
                 cityList.add(builder.build());
             }

        }
        return cityList;
    }


    private boolean isStartElement(int actual, String tagName, XMLStreamReader xmlStreamReader){
        return actual == XMLStreamReader.START_ELEMENT && tagName.equals(xmlStreamReader.getLocalName());
    }

    private boolean isEndELement(int actual, String tagName, XMLStreamReader xmlStreamReader){
        return actual == XMLStreamReader.END_ELEMENT && tagName.equals(xmlStreamReader.getLocalName());
    }

    private String randomHexColor(){
        return String.format("#%06X", new Random().nextInt(0x1000000));
    }
}
