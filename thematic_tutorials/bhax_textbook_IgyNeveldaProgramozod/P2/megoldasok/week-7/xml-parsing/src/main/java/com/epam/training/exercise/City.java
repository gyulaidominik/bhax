package com.epam.training.exercise;

    public class City {

        private String coordinateX;
        private String coordinateY;
        private String state;

        private City(CityBuilder city){
            this.coordinateX = city.coordinateX;
            this.coordinateY = city.coordinateY;
            this.state = city.state;
        }

        public String getCoordinateX() {
            return coordinateX;
        }

        public String getCoordinateY() {
            return coordinateY;
        }

        public String getState() {
            return state;
        }

        public static class CityBuilder{

            private String coordinateX;
            private String coordinateY;
            private String state;

            public CityBuilder coordinateX(String coordinateX){
                this.coordinateX = coordinateX;
                return this;
            }

            public CityBuilder coordinateY(String coordinateY){
                this.coordinateY = coordinateY;
                return this;
            }

            public CityBuilder state(String state){
                this.state = state;
                return this;
            }

            public City build(){
                return new City(this);
            }

        }

}
