public class MyException extends RuntimeException{

    private String errorCode;

    public MyException(String message, String errorCode){
        super(message);
        this.errorCode = errorCode;

    }


    public String getErrorCode() {
        return errorCode;
    }


    @Override
    public String toString() {
        return "MyException{" +
                "errorCode='" + errorCode + '\'' +
                '}';
    }
}
