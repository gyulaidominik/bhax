package abstractfactory;

import abstractfactory.Car.CarFactory;
import abstractfactory.Motorcycle.MotorcycleFactory;

public class FactoryProvider {

    public static AbstractFactoryInterface getFactory(String factory){

        if("Car".equalsIgnoreCase(factory)) return new CarFactory();
        if("Motorcycle".equalsIgnoreCase(factory)) return new MotorcycleFactory();

        return null;

    }

}
