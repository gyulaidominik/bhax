package abstractfactory;


import abstractfactory.Car.Car;
import abstractfactory.Motorcycle.Motorcycle;

public class Main {

    public static void main(String[] args) {

    AbstractFactoryInterface abstractFactory = FactoryProvider.getFactory("car");

    Car mySportCar = (Car) abstractFactory.create("sport");

    abstractFactory = FactoryProvider.getFactory("motorcycle");

    Motorcycle myChopperBike = (Motorcycle) abstractFactory.create("chopper") ;

    mySportCar.makeSound();
    myChopperBike.makeSound();





    }
}
