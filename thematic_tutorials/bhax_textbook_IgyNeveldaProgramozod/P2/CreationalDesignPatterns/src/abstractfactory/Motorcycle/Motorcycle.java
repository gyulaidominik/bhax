package abstractfactory.Motorcycle;

public interface Motorcycle {

    String recommendedUsage();
    void makeSound();


}
