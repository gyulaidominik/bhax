package abstractfactory.Motorcycle;

import abstractfactory.AbstractFactoryInterface;

public class MotorcycleFactory implements AbstractFactoryInterface {

    @Override
    public Motorcycle create(String str) {

        if("Chopper".equalsIgnoreCase(str)) return new Chopperbike();
        else if("Sport".equalsIgnoreCase(str)) return new Sportbike();

        return null;
    }

}
