package abstractfactory.Motorcycle;

public class Sportbike implements Motorcycle {

    public Sportbike(){
    }

    @Override
    public String recommendedUsage() {
        return "recommended for everyone";
    }

    @Override
    public void makeSound() {
        System.out.println("Prrrrrr...");
    }
}
