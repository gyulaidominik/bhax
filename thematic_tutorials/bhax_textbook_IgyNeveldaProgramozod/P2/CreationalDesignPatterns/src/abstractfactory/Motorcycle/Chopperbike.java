package abstractfactory.Motorcycle;

public class Chopperbike implements Motorcycle{

    public Chopperbike(){
    }

    @Override
    public String recommendedUsage() {
        return "recommended for rockers";
    }

    @Override
    public void makeSound() {
        System.out.println("BRGGRGRGRGRRGGR");
    }
}
