package abstractfactory.Car;

public class SportCar implements Car {


    public SportCar(){
    }

    @Override
    public String recommendedUsage() {
        return "Recommended for cool people";
    }

    @Override
    public void makeSound() {
        System.out.println("Vroooooom");
    }
}
