package abstractfactory.Car;

public class LuxuryCar implements Car{

    public LuxuryCar(){

    }


    @Override
    public String recommendedUsage() {
        return "recommended for classy people";
    }

    @Override
    public void makeSound() {
        System.out.println("Brrrrrrrrr");
    }
}
