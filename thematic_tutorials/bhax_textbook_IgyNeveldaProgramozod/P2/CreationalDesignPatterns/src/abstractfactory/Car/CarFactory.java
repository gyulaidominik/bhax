package abstractfactory.Car;

import abstractfactory.AbstractFactoryInterface;

public class CarFactory implements AbstractFactoryInterface {

    @Override
    public Car create(String str) {

        if("Sport".equalsIgnoreCase(str)) return new SportCar();
        else if("Luxury".equalsIgnoreCase(str)) return new LuxuryCar();

        return null;
    }
}
