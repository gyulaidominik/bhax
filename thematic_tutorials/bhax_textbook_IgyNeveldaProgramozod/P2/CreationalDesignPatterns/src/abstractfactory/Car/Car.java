package abstractfactory.Car;

public interface Car {

    String recommendedUsage();
    void makeSound();

}
