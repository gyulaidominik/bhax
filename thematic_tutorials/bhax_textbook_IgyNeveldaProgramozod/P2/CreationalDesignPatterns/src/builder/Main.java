package builder;

public class Main {

    public static void main(String[] args) {

        Person myPerson = new Person.PersonBuilder()
                                    .firstName("Dominik")
                                    .lastName("Gyulai")
                                    .age(20)
                                    .gender("Male")
                                    .profession("Student")
                                    .build();


        System.out.println(myPerson);
    }

}
