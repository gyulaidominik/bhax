package builder;

public class Person {

    private String firstName;
    private String lastName;
    private Integer age;
    private String gender;
    private String profession;

    private Person(PersonBuilder builder){
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.gender = builder.gender;
        this.profession = builder.profession;
    }

    public String getGender() {
        return gender;
    }

    public Integer getAge() {
        return age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfession() {
        return profession;
    }

    @Override
    public String toString() {
        return "Person: " +
                " firstName: '" + firstName + '\'' +
                ", lastName: '" + lastName + '\'' +
                ", age: " + age +
                ", gender: '" + gender + '\'' +
                ", profession: '" + profession + '\'';
    }

    public static class PersonBuilder{
        private String firstName;
        private String lastName;
        private Integer age;
        private String gender;
        private String profession;

        public PersonBuilder(){}

        public PersonBuilder firstName(String firstName){
            this.firstName = firstName;
            return this;
        }

        public PersonBuilder lastName(String lastName){
            this.lastName = lastName;
            return this;
        }

        public PersonBuilder age(Integer age){
            this.age = age;
            return this;
        }

        public PersonBuilder gender(String gender){
            this.gender = gender;
            return this;
        }

        public PersonBuilder profession(String profession){
            this.profession = profession;
            return this;
        }

        public Person build(){
            Person person = new Person(this);
            return person;
        }

    }















}
