package singleton;

public class Main {
    public static void main(String[] args) {

        Singleton singleton_1 = Singleton.getInstance("Singleton_1");
        Singleton singleton_2 = Singleton.getInstance("Singleton_2");

        System.out.println(singleton_1.value);

        System.out.println(singleton_2.value);

    }
}
