package factory;

public abstract class Mail {

    private String deliveryPlan;
    private String deliveryAddress;
    private Integer maxDeliveryTime;


    public void setDeliveryPlan(String newPlan){ this.deliveryPlan = newPlan; }
    public void setDeliveryAddress(String newDeliveryAddress) { this.deliveryAddress = newDeliveryAddress; }
    public void setDeliveryTime(Integer newDeliveryTime) { this.maxDeliveryTime = newDeliveryTime; }

    @Override
    public String toString() {
        return "Mail{" +
                "deliveryPlan='" + deliveryPlan + '\'' +
                ", deliveryAddress='" + deliveryAddress + '\'' +
                ", maxDeliveryTime=" + maxDeliveryTime +
                '}';
    }
}
