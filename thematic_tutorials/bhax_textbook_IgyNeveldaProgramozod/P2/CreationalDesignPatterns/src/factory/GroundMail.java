package factory;

public class GroundMail extends Mail {

    private static final int DELIVERY_TIME = 14;

    GroundMail(){
        setDeliveryAddress("Some address within 1500km");
        setDeliveryPlan("Deliver on ground");
        setDeliveryTime(DELIVERY_TIME);
    }


}
