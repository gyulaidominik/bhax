package factory;

public class MailFactory {

    public Mail createMail(int distance){

        if(distance < 1500 ) {
            return new GroundMail();
        }
        else {
            return new AirMail();
        }
    }


}
