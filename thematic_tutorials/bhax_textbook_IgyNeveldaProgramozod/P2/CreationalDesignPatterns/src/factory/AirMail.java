package factory;

public class AirMail extends Mail {

    private static final int DELIVERY_TIME = 21;

    AirMail(){
        setDeliveryAddress("Some address further than 1500km");
        setDeliveryPlan("Deliver in air");
        setDeliveryTime(DELIVERY_TIME);
    }





}
